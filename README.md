---
description: >-
  Once, Tyler was a technology-oriented teacher. Now, he's a teaching oriented
  technologist. Turns out, teaching people how to use their software well isn't
  so different from teaching Algebra!
cover: .gitbook/assets/Bard_Generated_Image.jpeg
coverY: 0
---

# 🧑💻 Teaching-oriented Technologist

📚 Tyler effectively transforms customer requests and issues into documentation and feature briefs. Artifact: "[No-Code Documentation: Building A Sustainable Framework](https://www.youtube.com/live/UQiS9SjF-zw?si=738e5so\_g8x5Nq5J)"

:love\_letter: Tyler invites and executes on continuous improvement cycles from himself and those he works with.  Artifact: "[Tyler Sellhorn: Local Teacher to Global Leader, Navigating Career Transition | Work 20XX #10](https://youtu.be/V9Zr4jrxOlw?si=IkremxUCKh\_yFMPm)"

:microphone: Tyler instructs and leads change management, even asynchronously. Artifact: "[The Remote Show with Dani Grant, CEO at Jam](https://theremoteshow.link/episodes/dani-grant-ceo-at-jam)"

