---
description: >-
  Tyler is software nerd that happens to be an outstanding generalist with the
  technical chops to accelerate improvements cross-functionally.
cover: .gitbook/assets/Bard_Generated_Image (1).jpeg
coverY: 33
---

# 🧰 Move fast & fix things

:gear: Tyler is a no-coder that can teach the robots how to do the robotic things and enable people to thrive in helping customers succeed. Artifact: [Using No-code to Scale Yourself (Spot-an-Op with Tyler Sellhorn)](https://youtu.be/Ea-76F\_JzW4?si=5h1fuVSNkP-Zh2UJ)

:screwdriver: Tyler helps others unlock the power of asynchronous communication and building shared understanding states. Artifact: [Asynchronous audio meetings w/ YAC on the Remote First Podcast](https://podcasters.spotify.com/pod/show/remotefirst/episodes/Bonus---How-to-run-asynchronous-audio-meetings-w-Tyler-Sellhorn--YAC--Startup-Series-e18sfjf)

:loudspeaker: Tyler is an enthusiastic, empathetic leader with track record of improving organizations and helping them achieve their goals. Artifact: [Hubstaff CEO requesting team culture GIFs years later](https://share.cleanshot.com/tdyGL9PV)
