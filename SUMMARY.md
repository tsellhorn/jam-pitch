# Table of contents

* [🧑💻 Teaching-oriented Technologist](README.md)
* [📈 Outcomes-oriented CX leader](outcomes-oriented-cx-leader.md)
* [🧰 Move fast & fix things](move-fast-and-fix-things.md)
