---
description: >-
  Tyler has twice been the 1st CX leader hire at image & screen recording
  centered SaaS companies (aka Jam). He effectively asks and answers: what is
  the customer trying to accomplish with our software?
cover: .gitbook/assets/Twitter Purple Nautical.png
coverY: 0
---

# 📈 Outcomes-oriented CX leader

:record\_button: Tyler can quickly adapt to changing requirements and market conditions. Artifact: [Rapid change management on the Agile in Action Podcast with Bill Raymond](https://agileinaction.com/agile-in-action-podcast/2021/03/02/tyler-sellhorn-talks-about-rapid-change-management.html)

:a: Tyler leverages relationships and social media to influence adoption and adaptation to new workflows. Artifact: [Debunking Remote's Biggest Myths on Almanac's The Big Bet](https://thebigbet.beehiiv.com/p/tyler-sellhorn-head-of-remote)

:bar\_chart: Tyler thinks strategically and systematically to produce step change improvements and decision making. Artifact: [Customer Success Priorities in 2022](https://youtu.be/M5cYcX5axQg?si=anTW8In5t1KVqFkh)
